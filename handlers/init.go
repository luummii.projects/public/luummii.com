package handlers

import (
	"github.com/qor/render"
	"projects/league.dance/database"
)

// Handler struct for work with routers
type Handler struct {
	db       *database.DataBase
	renderer *render.Render
}

// Init - new makes use of the service to provide an http.Handler with predefined routing.
func Init(db *database.DataBase, renderer *render.Render) *Handler {
	h := &Handler{
		db:       db,
		renderer: renderer,
	}
	return h
}
