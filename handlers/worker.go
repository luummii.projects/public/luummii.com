package handlers

import "github.com/gin-gonic/gin"

// HandlersWorker - is func for work with path query
func (h *Handler) HandlersWorker(router *gin.Engine) {

	// For render temmplate worker
	router.GET("/", h.tmpl)
	router.GET("/news", h.tmpl)
	router.GET("/rules", h.tmpl)

	// For API worker
	router.POST("/getdata", h.getEXAMPLEdata)
}

// For paste template home in application
func (h *Handler) tmpl(ctx *gin.Context) {
	h.renderer.Execute(
		"home",
		gin.H{},
		ctx.Request,
		ctx.Writer,
	)
}
