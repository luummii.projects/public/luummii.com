package main

import (
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-webpack/webpack"
	"github.com/joho/godotenv"
	"github.com/qor/render"
	"projects/league.dance/database"
	"projects/league.dance/handlers"
)

var (
	isDev    *bool
	renderer *render.Render
)

func init() {
	// Init ENV variables
	if err := godotenv.Load("config.env"); err != nil {
		log.Panicf("ERROR READ CONFIG.ENV:%s\n", err)
	}

	// Logging to a file.
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f)

	// Gin debug mode.
	gin.SetMode(gin.DebugMode) // FIX Need change on gin.ReleaseMode

	// For webpack reloader
	isDev = flag.Bool("dev", false, "development mode")
	flag.Parse()

	// For render templates
	renderer = render.New(&render.Config{
		ViewPaths: []string{"templates"},
		FuncMapMaker: func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
			return map[string]interface{}{"asset": webpack.AssetHelper}
		},
	})
}

func webpackInit() {
	webpack.FsPath = "./public/assets"
	webpack.WebPath = "manifest"
	webpack.Plugin = "manifest"
	webpack.Init(*isDev)
}

func main() {
	// Init database and if need make migration
	db := database.Init()
	defer func() {
		db.DB.Close()
		log.Println("DATABASE IS CLOSE")
	}()

	// Create new gin:)
	r := gin.Default()
	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())
	// Serving static files
	r.Use(static.Serve("/", static.LocalFile("public", false)))

	// For webpack hot reload and add path to assets in template
	webpackInit()

	// Init handlers for interaction between query data and database
	h := handlers.Init(db, renderer)
	h.HandlersWorker(r)

	// Rin server on port 5000
	port := os.Getenv("PORT")
	if port == "" {
		log.Panicln("$PORT must be set")
	}
	r.Run(":" + port)
}
