'use strict'

var path = require('path')
var webpack = require('webpack')
var ManifestPlugin = require('webpack-manifest-plugin')
var CompressionPlugin = require("compression-webpack-plugin")
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CopyWebpackPlugin = require("copy-webpack-plugin")

var host = process.env.HOST || 'localhost'
var devServerPort = 8080

var production = process.env.NODE_ENV === 'production'

const ExtractCssChunks = require("extract-css-chunks-webpack-plugin")

class CleanUpExtractCssChunks {
  shouldPickStatChild(child) {
    return child.name.indexOf('extract-css-chunks-webpack-plugin') !== 0
  }

  apply(compiler) {
    compiler.hooks.done.tap('CleanUpExtractCssChunks', (stats) => {
      const children = stats.compilation.children;
      if (Array.isArray(children)) {
        // eslint-disable-next-line no-param-reassign
        stats.compilation.children = children
          .filter(child => this.shouldPickStatChild(child));
      }
    });
  }
}
var config = {
  //stats: { children: false },
  mode: production ? "production" : "development",
  entry: {
    // Sources are expected to live in $app_root/webpack
    application: [
      './assets/js/application.js',
      './assets/scss/application.scss'
    ]
  },
  module: {
    rules: [
      { test: /\.vue/, use: "vue-loader" },
      { test: /\.es6$/, use: "babel-loader" },
      { test: /\.jsx$/, use: "babel-loader" },
      { test: /\.(jpe?g|png|gif)$/i, use: "file-loader" },
      {
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },{
          loader: 'expose-loader',
          options: '$'
        }]
      },
      {
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)|\.otf($|\?)/,
        //use: production ? 'file-loader' : 'url-loader'
        use: 'file-loader'
      },
      {
        test: /\.(sass|scss|css)$/,
        use: [
          ExtractCssChunks.loader,
          {
            loader: "css-loader",
            options: {
              minimize: true,
              sourceMap: true
            }
          },
          {
            loader: "sass-loader"
          }
        ]
      },
    ]
  },

  output: {
    // Build assets directly in to public/webpack/, let webpack know
    // that all webpacked assets start with webpack/

    // must match config.webpack.output_dir
    path: path.join(__dirname, 'public', 'assets'),
    publicPath: '/assets/',

    filename: production ? '[name]-[chunkhash].js' : '[name].js',
    chunkFilename: production ? '[name]-[chunkhash].js' : '[name].js',
  },

  resolve: {
    modules: [path.resolve(__dirname, "assets"), path.resolve(__dirname, "node_modules")],
    extensions: [".es6", ".jsx", ".sass", ".css", ".js"],
    alias: {
      '~': path.resolve(__dirname, "assets"),
      'vue$': `${__dirname}/node_modules/vue/dist/vue.esm.js`,
      'router$': `${__dirname}/node_modules/vue-router/dist/vue-router.esm.js`,
      'TweenLite': `${__dirname}/node_modules/gsap/TweenLite.js`
    }
  },

  plugins: [
    new CopyWebpackPlugin([{ from: "./assets", to: "" }], { copyUnmodified: true, ignore: ["scss/**", "js/**", "src/**"] }),
    new VueLoaderPlugin(),
    new ExtractCssChunks(
      {
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: production ? "[name]-[chunkhash].css" : "[name].css",
        chunkfilename: production ? "[name]-[id].css" : "[name].css",
        hot: production ? false : true,
      }
    ),
    new CleanUpExtractCssChunks(),
    new ManifestPlugin({
      writeToFileEmit: true,
      //basePath: "",
      publicPath: production ? "/assets/" : 'http://' + host + ':' + devServerPort + '/assets/',
    }),
    //new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru|en/),
    new webpack.ProvidePlugin({ $: "jquery", jQuery: "jquery" })
  ],
  optimization: {
    minimize: production,
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: {
          test: /[\\/]node_modules[\\/].*js/,
          priority: 1,
          name: "vendor",
          chunks: "initial",
          enforce: true
        },
      },
    },
  }
};

if (production) {
  config.plugins.push(
    //new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({ // <--key to reduce React's size
      'process.env': { NODE_ENV: JSON.stringify('production') }
    }),
    new CompressionPlugin({
      //asset: "[path].gz",
      algorithm: "gzip",
      test: /\.js$|\.css$/,
      threshold: 4096,
      minRatio: 0.8
    })
  );
  config.output.publicPath = '/assets/';
} else {
  config.plugins.push(
    new webpack.NamedModulesPlugin(),
  )

  config.devServer = {
    port: devServerPort,
    disableHostCheck: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
  };

  config.output.publicPath = 'http://' + host + ':' + devServerPort + '/assets/';
  // Source maps
  config.devtool = 'source-map';
}

module.exports = config
