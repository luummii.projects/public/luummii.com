import Vue from 'vue'
import VueRouter from 'vue-router'

import './libs/bootstrap/css/bootstrap.min.css'
import './libs/rs-plugin/css/settings.css'
import './libs/owl-carousel/owl.carousel.css'

import './libs/bootstrap/js/bootstrap.min.js'
import './libs/rs-plugin/js/revolution-slider.js'
import './libs/owl-carousel/owl.carousel.min.js'
import './libs/plugins.min.js'
import './libs/main.js'

import App from './App.vue'
import UiMain from './view/ui-main.vue'
import UiNews from './view/ui-news.vue'
import UiRules from './view/ui-rules.vue'

const routes = [
  {
    path: '/', component: UiMain
  },
  {
    path: '/news', component: UiNews
  },
  {
    path: '/rules', component: UiRules
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

Vue.use(VueRouter)

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})